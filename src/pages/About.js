import '../App.css';
import Language from '../components/LanguageComponent';
import React, { Component } from 'react';

class About extends Component {
  LanguageList = {
    list: [
      {
        id: 'HTML & CSS',
        name: 'HTML & CSS',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg'
      },
      {
        id: 'JavaScript',
        name: 'JavaScript',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg'
      },
      {
        id: 'React',
        name: 'React',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg'
      },
      {
        id: 'Ruby',
        name: 'Ruby',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg'
      },
      {
        id: 'Ruby on Rails',
        name: 'Ruby on Rails',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg'
      },
      {
        id: 'Python',
        name: 'Python',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg'
      }
    ]
  };

  ListLanguage = (language) => {
    return (
      language.map((languageItem) => {
        if (this.props.id === languageItem.name) {
          return (
            <Language
              name={languageItem.name}
              image={languageItem.image}
              isBtn={false}
            />
          )
        } else if (this.props.id === undefined) {
          return (
            <Language
              name={languageItem.name}
              image={languageItem.image}
              isBtn={true}
            />
          )
        }
      })
    )
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div class="language-card">
            {this.ListLanguage(this.LanguageList.list)}
          </div>
        </header>
      </div>
    );
  }
}

export default About;
