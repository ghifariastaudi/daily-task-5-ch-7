import React from 'react';
import { Link } from "react-router-dom";
import '../App.css';
import Button from '@mui/material/Button';

class Language extends React.Component {
  render() {
    return (
      <div className='language-item'>
        <div className='language-name'>{this.props.name}</div>
        <img className='language-image'src={this.props.image} 
        />
        {this.props.isBtn ? (
            <Link
            to={{
              pathname: `/language/${this.props.name}`
            }}
          >
            <Button className='button' variant="contained">VIEW</Button>
          </Link>  
        ) : (<></>)}

      </div>
    );
  }
}

export default Language;
